var maggiObj = (function() {
	

	var $section 	 = $('.section'),
		$body		 = $('body'),
		$mainMenu		 = $('#main-menu'),
		$mmenuBtn 	 = $('#mmenubtn'),
		$mmCloseBtn	 = $('.mm-close'),
		$mmenu 		 = $('#maggi-menu'),
		$mmenu 		 = $('#maggi-menu'),
		API;	



	function init() {
		
		initMmenu();
		//mainMenuMobile();
		dishSimilarSlider();

		$mmCloseBtn.on('click', mmenuCloseHandle );
		$('.mbtn').on('click', mmenuCloseHandle );





	}


	//preLoader
	function preLoader(){
		
	}

	//mmenuCloseHandle
	function mmenuCloseHandle(e){
		e.preventDefault();
		API.close();
	}

	//mobile menu
	function initMmenu(){
		$mmenu.mmenu({
          	offCanvas : {
	        	position	: "top",
				zposition	: "front"
         	}
      	});
      	API = $mmenu.data( "mmenu" );
	}

	//
	function mainMenuMobile(){
		if($('html').hasClass('mobile') || $( window ).width() < 768 ){
			$mainMenu.addClass('navbar-fixed-top');

		}else{
			$mainMenu.removeClass('navbar-fixed-top');
		}
	}


	//preLoader
	function dishSimilarSlider(){
		if(!$body.hasClass('home')) return;

		 $('.slick-dishes').slick({
	        	infinite: true,
			  	adaptiveHeight: true,
			  	arrows: true,
  				mobileFirst:true,

  				responsive: [
				    {
				      breakpoint: 768,
				      settings: {
				        arrows: true,				       
				        slidesToShow: 3
				      }
				    },
				    {
				      breakpoint: 480,
				      settings: {
				        arrows: false,				       
				        slidesToShow: 1
				      }
				    }
				  ]
	      });

	}


	




	
	



	return { init : init };

})();

maggiObj.init();