var app = angular.module('app', []);

app.service('mnuPlnSev', ['$http','$q', function($http,$q){
	var url = "recipes.json";
	var menu = this;

	menu.getMenu = function(){

		var defer = $q.defer();
		$http.get(url).success(function(res){
			defer.resolve(res)
		}).error(function(err,stt){
			defer.reject(err);
		})

		return defer.promise;
	}


}]);



app.controller('mnuPlnCtrl', ['$scope', 'mnuPlnSev', function($scope, mnuPlnSev) {
   

    $scope.currentWeek = 0 ;
    $scope.menuPlanner = [];
    
    $scope.week = [];

    $scope.mon = [];
    $scope.tue = [];
    $scope.wed = [];
    $scope.thu = [];
    $scope.fri = [];
    $scope.sat = [];
    $scope.sun = [];

    $scope.flag = false;


    mnuPlnSev.getMenu()
        .then(function(res){ 
            $scope.menuPlanner = res;
        
        }, function(err){
        	console.log(err)
    }); 

    $scope.getWeekNumber = function () {
        var d       = new Date();
        var date    = d.getDate();
        var day     = d.getDay();

        return Math.ceil((date - 1 - day)/7) - 1;      
    };
   

    $scope.getWeekCurrent = function() {
            
        if($scope.flag == true){
            return;
        }

        var n = $scope.getWeekNumber();    

        angular.forEach($scope.menuPlanner, function(value,key){
            if(value.week == n ){
                $scope.week = value.recipes;
            } 
        });       
        
        $scope.groupbyDay();
        $scope.currentWeek = n;
        //console.log("W0 init  " + $scope.currentWeek);
    };   


    $scope.getWeek = function() {     

        angular.forEach($scope.menuPlanner, function(value,key){ 

            if(value.week == $scope.currentWeek ){
                $scope.week = value.recipes;
            } 

        }); 

        $scope.groupbyDay();
        //console.log("currentW1  " + $scope.currentWeek);  

        $scope.flag = true;

    };   




    $scope.groupbyDay = function(){

       //console.log("currentW3  " + $scope.currentWeek);         

       $scope.mon = [];
       $scope.tue = [];
       $scope.wed = [];
       $scope.thu = [];
       $scope.fri = [];
       $scope.sat = [];
       $scope.sun = [];

       for ( var i = 0 ; i < $scope.week.length ; i++ ) {
            
            if( $scope.week[i].weekday == 'sun'){
                $scope.sun.push($scope.week[i]);
            }

            if( $scope.week[i].weekday == 'mon'){
                $scope.mon.push($scope.week[i]);
            } 

            if( $scope.week[i].weekday == 'tue'){
                $scope.tue.push($scope.week[i]);
            }

            if( $scope.week[i].weekday == 'wed'){
                $scope.wed.push($scope.week[i]);
            }

            if( $scope.week[i].weekday == 'thu'){
                $scope.thu.push($scope.week[i]);
            }

            if( $scope.week[i].weekday == 'fri'){
                $scope.fri.push($scope.week[i]);
            }

            if( $scope.week[i].weekday == 'sat'){
                $scope.sat.push($scope.week[i]);
            }

       }

    }



    $scope.prevWeek = function(){

        if ($scope.currentWeek > 0) { 
            $scope.currentWeek-- ; 
            //console.log("currentW4 - prevWeek " + $scope.currentWeek );   
            $scope.getWeek();  
        }
       
    };

    $scope.nextWeek = function(){
        if ($scope.currentWeek < 3 ) { 
            $scope.currentWeek++ ;
             //console.log("currentW5 - nextWeek " + $scope.currentWeek );   
            $scope.getWeek(); 
        }        
    };

    $scope.prevPageDisabled = function() {
        return $scope.currentWeek === 0 ? "disabled" : "";
    };

    $scope.nextPageDisabled = function() {
        return $scope.currentWeek === 3 ? "disabled" : "";
    
    };  


  





}]);    